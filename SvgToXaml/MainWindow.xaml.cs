﻿using MiDiUm.WPF.UI.Controls;
using SvgToXaml.Properties;
using SvgToXaml.ViewModels;
using System.ComponentModel;

namespace SvgToXaml
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow: MiDiUmWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new SvgImagesViewModel();
            ((SvgImagesViewModel)DataContext).CurrentDir = Settings.Default.LastDir;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            //Save current Dir for next Start
            Settings.Default.LastDir = ((SvgImagesViewModel)DataContext).CurrentDir;
            Settings.Default.Save();

            base.OnClosing(e);
        }

    }
}