﻿using System;

namespace SvgToXaml
{
    internal static class Program
    {
        [STAThread]
        private static int Main()
        {
            int exitCode = 0;
            var app = new App();
            app.InitializeComponent();
            app.Run();
            return exitCode;
        }
    }
}