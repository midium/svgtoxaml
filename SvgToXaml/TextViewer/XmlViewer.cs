﻿using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Highlighting;
using System.Windows;

namespace SvgToXaml.TextViewer
{
    public class XmlViewer : TextEditor
    {
        #region Public Fields

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            nameof(Text), typeof(string), typeof(XmlViewer), new PropertyMetadata(default(string), TextChanged));

        #endregion Public Fields

        #region Public Constructors

        public XmlViewer()
        {
            SyntaxHighlighting = HighlightingManager.Instance.GetDefinition("XML");
            Options.EnableHyperlinks = true;
            Options.EnableEmailHyperlinks = true;

            ShowLineNumbers = true;

        }

        #endregion Public Constructors

        #region Public Properties

        public new string Text
        {
            get { return Document.Text; }
            set { SetValue(TextProperty, value); }
        }

        #endregion Public Properties

        #region Private Methods

        private new static void TextChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
        {
            var xmlViewer = (XmlViewer)dependencyObject;
            if (args == null || args.NewValue == null)
                xmlViewer.Document.Text = string.Empty;
            else
                xmlViewer.Document.Text = (string)args.NewValue;
        }

        #endregion Private Methods
    }
}