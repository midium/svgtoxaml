using System.Windows;

namespace SvgToXaml.Explorer
{
    public static class TreeViewItemProps
    {
        #region Public Fields

        public static readonly DependencyProperty IsRootLevelProperty =
            DependencyProperty.RegisterAttached(
            "IsRootLevel",
            typeof(bool),
            typeof(TreeViewItemProps),
            new UIPropertyMetadata(false));

        #endregion Public Fields

        #region Public Methods

        public static bool GetIsRootLevel(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsRootLevelProperty);
        }

        public static void SetIsRootLevel(
            DependencyObject obj, bool value)
        {
            obj.SetValue(IsRootLevelProperty, value);
        }

        #endregion Public Methods
    }
}