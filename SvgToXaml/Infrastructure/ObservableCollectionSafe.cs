﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;

namespace SvgToXaml.Infrastructure
{
    /// <summary>
    /// ObservableCollection, which is thread safe
    /// Use:
    /// BindingOperations.EnableCollectionSynchronization (Items, null, Items.HandleSynchonizationCallback);
    /// As a result, the framework takes care of the correct synchronization of the callbacks, i. One can also
    /// in the thread to access the list and both the Listkram, as well as the notification and
    /// Updating the GUI works.
    /// </summary>
    [DebuggerDisplay("Count = {Count}")]
    public class ObservableCollectionSafe<T> : INotifyCollectionChanged, INotifyPropertyChanged, IList<T>, IList, IReadOnlyList<T>, IDisposable
    {
        #region Internal Fields

        internal const string CountString = "Count";
        internal const string IndexerName = "Item[]";

        // ReSharper disable once InconsistentNaming
        internal ReaderWriterLockSlim _lock;

        #endregion Internal Fields

        #region Private Fields

        private readonly Collection<T> _coll;
        private volatile bool _inBatchUpdate;
        private volatile bool _isInCollectionChanged;

        #endregion Private Fields

        #region Constructor

        public ObservableCollectionSafe()
        {
            _coll = new Collection<T>();
            _lock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        }

        public ObservableCollectionSafe(IList<T> list)
            : this()
        {
            CopyFrom(list);
        }

        public ObservableCollectionSafe(IEnumerable<T> items)
            : this()
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));
            CopyFrom(items);
        }

        private void CopyFrom(IEnumerable<T> items)
        {
            if (items != null)
            {
                using (IEnumerator<T> enumerator = items.GetEnumerator())
                {
                    while (enumerator.MoveNext())
                    {
                        _coll.Add(enumerator.Current);
                    }
                }
            }
        }

        #endregion Constructor

        #region Public Indexers

        public T this[int index]
        {
            get
            {
                return ReadAccess(() => _coll[index]);
            }
            set
            {
                WriteAccess(false, (ref object dummy) =>
                {
                    T originalItem = this[index];
                    _coll[index] = value;
                    return new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, originalItem, value, index);
                });
            }
        }

        #endregion Public Indexers

        #region Public Methods

        public void Dispose()
        {
            _lock.Dispose();
        }

        #endregion Public Methods

        #region Events

        public virtual event NotifyCollectionChangedEventHandler CollectionChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            var handler = CollectionChanged;
            if (handler != null)
            {
                _isInCollectionChanged = true;
                try
                {
                    handler(this, e);
                }
                finally
                {
                    _isInCollectionChanged = false;
                }
            }
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }

        private void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        #endregion Events

        #region Write Access

        private delegate NotifyCollectionChangedEventArgs WriteAccessFunc<TResult>(ref TResult result);

        public int Add(T item)
        {
            // ReSharper disable once RedundantAssignment
            return WriteAccess(true, (ref int index) =>
            {
                _coll.Add(item);
                index = _coll.Count - 1;
                return new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index);
            });
        }

        public void AddRange(IEnumerable<T> items)
        {
            WriteAccess(true, (ref object dummy) =>
            {
                foreach (var item in items)
                {
                    _coll.Add(item);
                }
                return new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
            });
        }

        public IDisposable BatchUpdate()
        {
            if (_inBatchUpdate)
                throw new Exception("BatchUpdate already in progress");
            _inBatchUpdate = true;
            return new CustomDisposable(() =>
            {
                _inBatchUpdate = false;
                OnPropertyChanged(CountString);
                OnPropertyChanged(IndexerName);
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            });
        }

        public void Clear()
        {
            WriteAccess(true, (ref object dummy) =>
            {
                _coll.Clear();
                return new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
            });
        }

        public void Insert(int index, T item)
        {
            WriteAccess(true, (ref object dummy) =>
            {
                _coll.Insert(index, item);
                return new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index);
            });
        }

        public void Move(int oldIndex, int newIndex)
        {
            WriteAccess(false, (ref object dummy) =>
            {
                T removedItem = _coll[oldIndex];

                _coll.RemoveAt(oldIndex);
                _coll.Insert(newIndex, removedItem);
                return new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Move, removedItem, newIndex, oldIndex);
            });
        }

        public bool Remove(T item)
        {
            // ReSharper disable once RedundantAssignment
            return WriteAccess(true, (ref bool result) =>
            {
                int index = _coll.IndexOf(item);
                if (index < 0)
                {
                    result = false;
                    return null;
                }
                _coll.RemoveAt(index);
                result = true;
                return new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index);
            });
        }

        public void RemoveAt(int index)
        {
            WriteAccess(true, (ref object dummy) =>
            {
                T removedItem = _coll[index];
                _coll.RemoveAt(index);
                return new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, removedItem, index);
            });
        }

        public void RemoveRange(IEnumerable<T> items)
        {
            WriteAccess(true, (ref object dummy) =>
            {
                foreach (var item in items)
                {
                    _coll.Remove(item);
                }
                return new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
            });
        }

        private TResult WriteAccess<TResult>(bool countChanged, WriteAccessFunc<TResult> action)
        {
            TResult result = default(TResult);
            _lock.EnterWriteLock();
            try
            {
                NotifyCollectionChangedEventArgs args = action(ref result);

                if (args == null)
                    return result; //skip Update
                if (!_inBatchUpdate)
                {
                    if (countChanged)
                        OnPropertyChanged(CountString);
                    OnPropertyChanged(IndexerName);
                    OnCollectionChanged(args);
                }
                return result;
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        #endregion Write Access

        #region Read Access

        public int Count
        {
            get
            {
                return ReadAccess(() => _coll.Count);
            }
        }

        public bool Contains(T item)
        {
            return ReadAccess(() => _coll.Contains(item));
        }

        public void CopyTo(T[] array, int index)
        {
            ReadAccess<object>(() =>
            {
                _coll.CopyTo(array, index);
                return null;
            });
        }

        public void CopyTo(Array array, int index)
        {
            ReadAccess<object>(() =>
            {
                ((ICollection)_coll).CopyTo(array, index);
                return null;
            });
        }

        public int IndexOf(T item)
        {
            return ReadAccess(() => _coll.IndexOf(item));
        }

        private TResult ReadAccess<TResult>(Func<TResult> action)
        {
            _lock.EnterReadLock();
            try
            {
                return action();
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }

        #endregion Read Access

        public void HandleSynchonizationCallback(IEnumerable collection, object context, Action accessMethod, bool writeAccess)
        {
            if (!Equals(collection))
                throw new Exception("Collection does not match");
            if (_isInCollectionChanged)
            {
                if (writeAccess && !_lock.IsWriteLockHeld)
                    throw new Exception("When calling CollectionChanged you should use Write Lock");
                accessMethod();
            }
            else
            {
                // is called at startup, and e.g. when clear in the thread, then the refresh comes later here,
                // with simple add etc. you can not get through here, this makes the shadow list all by itself
                if (writeAccess)
                    _lock.EnterWriteLock();
                else
                    _lock.EnterReadLock();

                accessMethod();

                if (writeAccess)
                    _lock.ExitWriteLock();
                else
                    _lock.ExitReadLock();
            }
        }

        public class CustomDisposable : IDisposable
        {
            #region Private Fields

            private readonly Action _action;

            #endregion Private Fields

            #region Public Constructors

            public CustomDisposable(Action action)
            {
                _action = action;
            }

            #endregion Public Constructors

            #region Public Methods

            public void Dispose()
            {
                _action();
            }

            #endregion Public Methods
        }

        #region Interface stuff

        int ICollection<T>.Count => Count;

        int ICollection.Count => Count;

        int IReadOnlyCollection<T>.Count => Count;

        bool IList.IsFixedSize => ((IList)_coll).IsFixedSize;

        bool ICollection<T>.IsReadOnly => ((IList<T>)_coll).IsReadOnly;

        bool IList.IsReadOnly => ((IList)_coll).IsReadOnly;

        bool ICollection.IsSynchronized => ((IList)_coll).IsFixedSize;

        object ICollection.SyncRoot
        {
            get { throw new NotSupportedException("This ObservableCollection doesn't need external synchronization"); }
        }

        T IList<T>.this[int index]
        {
            get
            {
                return this[index];
            }
            set
            {
                this[index] = value;
            }
        }

        object IList.this[int index]
        {
            get
            {
                return this[index];
            }
            set
            {
                this[index] = (T)value;
            }
        }

        T IReadOnlyList<T>.this[int index] => this[index];

        void ICollection<T>.Add(T item)
        {
            Add(item);
        }

        int IList.Add(object value)
        {
            return Add((T)value);
        }

        void ICollection<T>.Clear()
        {
            Clear();
        }

        void IList.Clear()
        {
            Clear();
        }

        bool ICollection<T>.Contains(T item)
        {
            return Contains(item);
        }

        bool IList.Contains(object value)
        {
            return Contains((T)value);
        }

        void ICollection<T>.CopyTo(T[] array, int arrayIndex)
        {
            CopyTo(array, arrayIndex);
        }

        void ICollection.CopyTo(Array array, int index)
        {
            CopyTo(array, index);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return ReadAccess(() => new List<T>(_coll).GetEnumerator()); //return a copy
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<T>)this).GetEnumerator();
        }

        int IList<T>.IndexOf(T item)
        {
            return IndexOf(item);
        }

        int IList.IndexOf(object value)
        {
            return IndexOf((T)value);
        }

        void IList<T>.Insert(int index, T item)
        {
            Insert(index, item);
        }

        void IList.Insert(int index, object value)
        {
            Insert(index, (T)value);
        }

        bool ICollection<T>.Remove(T item)
        {
            return Remove(item);
        }

        void IList.Remove(object value)
        {
            Remove((T)value);
        }

        void IList<T>.RemoveAt(int index)
        {
            RemoveAt(index);
        }

        void IList.RemoveAt(int index)
        {
            RemoveAt(index);
        }

        #endregion Interface stuff
    }
}