﻿using SvgConverter;
using SvgToXaml.Command;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace SvgToXaml.ViewModels
{
    public class SvgImageViewModel : ImageBaseViewModel
    {
        #region Private Fields

        private ConvertedSvgData _convertedSvgData;

        #endregion Private Fields

        #region Public Constructors

        public SvgImageViewModel(string filepath) : base(filepath)
        {
            OnPropertyChanged(nameof(Xaml));
            CopyToClipboardCommand = new DelegateCommand(CopyToClipboard);
        }

        public SvgImageViewModel(ConvertedSvgData convertedSvgData)
            : this(convertedSvgData.Filepath)
        {
            _convertedSvgData = convertedSvgData;
            CopyToClipboardCommand = new DelegateCommand(CopyToClipboard);
        }

        #endregion Public Constructors

        #region Public Properties

        public static SvgImageViewModel DesignInstance
        {
            get
            {
                var imageSource = new DrawingImage(new GeometryDrawing(Brushes.Black, null, new RectangleGeometry(new Rect(new Size(10, 10)), 1, 1)));
                var data = new ConvertedSvgData { ConvertedObj = imageSource, Filepath = "FilePath", Svg = "<svg/>", Xaml = "<xaml/>" };
                return new SvgImageViewModel(data);
            }
        }

        public ICommand CopyToClipboardCommand { get; internal set; }

        public string Svg => SvgData?.Svg;

        public ConvertedSvgData SvgData
        {
            get
            {
                if (_convertedSvgData == null)
                {
                    try
                    {
                        _convertedSvgData = ConverterLogic.ConvertSvg(Filepath, ResultMode.DrawingImage);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
                return _convertedSvgData;
            }
        }

        public string Xaml => SvgData?.Xaml;

        #endregion Public Properties

        #region Protected Methods

        protected override ImageSource GetImageSource()
        {
            return SvgData?.ConvertedObj as ImageSource;
        }

        protected override string GetSvgDesignInfo()
        {
            if (PreviewSource is DrawingImage)
            {
                var di = (DrawingImage)PreviewSource;
                if (di.Drawing is DrawingGroup)
                {
                    var dg = (DrawingGroup)di.Drawing;
                    var bounds = dg.ClipGeometry?.Bounds ?? dg.Bounds;
                    return $"{bounds.Width:#.##}x{bounds.Height:#.##}";
                }
            }
            return null;
        }

        #endregion Protected Methods

        #region Private Methods

        private void CopyToClipboard()
        {
            Clipboard.SetText(Xaml);
        }

        #endregion Private Methods
    }
}