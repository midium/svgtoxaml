﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Threading;

namespace SvgToXaml.ViewModels
{
    public abstract class ViewModelBase : BindableBase
    {
        #region Protected Properties

        protected static bool InDesignMode => DesignerProperties.GetIsInDesignMode(new DependencyObject());

        #endregion Protected Properties

        #region Public Methods

        /// <summary>
        /// Executes the action via the UI dispatcher.
        /// </summary>
        /// <param name="action">The action to run</param>
        public void InUi(Action action)
        {
            Application.Current.Dispatcher.BeginInvoke(action, DispatcherPriority.Background);
        }

        /// <summary>
        /// Executes the action via the UI dispatcher.
        /// </summary>
        /// <param name="priority">Priority</param>
        /// <param name="action">Action to run</param>
        public void InUi(DispatcherPriority priority, Action action)
        {
            Application.Current.Dispatcher.BeginInvoke(action, priority);
        }

        #endregion Public Methods
    }
}