using SvgToXaml.Command;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;

namespace SvgToXaml.ViewModels
{
    public abstract class ImageBaseViewModel : ViewModelBase
    {
        #region Private Fields

        private Stretch _imageStretch = Stretch.Uniform;

        #endregion Private Fields

        #region Protected Constructors

        protected ImageBaseViewModel(string filepath)
        {
            Filepath = filepath;
            OpenFileCommand = new DelegateCommand(OpenFileExecute);
            ToggleStrechCommand = new DelegateCommand(ToggleStrech);
        }

        #endregion Protected Constructors

        #region Public Properties

        public string Filename => Path.GetFileName(Filepath);
        public string Filepath { get; }
        public Stretch ImageStretch
        {
            get => _imageStretch;
            set => SetProperty(ref _imageStretch, value);
        }

        public ICommand OpenFileCommand { get; set; }
        public ImageSource PreviewSource => GetImageSource();
        public string SvgDesignInfo => GetSvgDesignInfo();
        public ICommand ToggleStrechCommand { get; set; }

        #endregion Public Properties

        #region Protected Methods

        protected abstract ImageSource GetImageSource();
        protected abstract string GetSvgDesignInfo();

        #endregion Protected Methods

        #region Private Methods

        private void OpenFileExecute()
        {
            Process.Start(Filepath);
        }

        private void ToggleStrech()
        {
            var values = Enum.GetValues(typeof(Stretch)).OfType<Stretch>().ToList();
            var idx = values.IndexOf(_imageStretch);
            idx = (idx + 1) % values.Count;
            ImageStretch = values[idx];
        }

        #endregion Private Methods
    }
}