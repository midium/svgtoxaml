﻿using MiDiUm.WPF.UI.Controls;
using System.Windows;

namespace SvgToXaml.Dialogs
{
    /// <summary>
    /// Interaction logic for InfoBox.xaml
    /// </summary>
    public partial class InfoBox : MiDiUmWindow
    {
        public InfoBox(): base()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
