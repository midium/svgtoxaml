﻿using BKLib.CommandLineParser;
using System;

namespace SvgConverter
{
    public static class CmdLineHandler
    {
        #region Public Methods

        public static int HandleCommandLine(string arg)
        {
            string[] args = arg != null ? arg.Split(' ') : new string[0];
            return HandleCommandLine(args);
        }

        public static int HandleCommandLine(string[] args)
        {
            var clp = new CommandLineParser { SkipCommandsWhenHelpRequested = true };

            clp.Target = new CmdLineTarget();
            clp.Header = "SvgToXaml - Tool to convert SVGs to a Dictionary\r\n(c) 2018 Matteo Loro";
            clp.LogErrorsToConsole = true;
            try
            {
                return clp.ParseArgs(args, true);
            }
            catch (Exception)
            {
                //nothing to do, the errors are hopefully already reported via CommandLineParser
                Console.WriteLine("Error while handling Command line.");
                return -1;
            }
        }

        #endregion Public Methods
    }
}