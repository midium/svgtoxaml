# SvgToXaml
This is a small C# WPF application capable to convert SVG files into XAML format to be used inside WPF projects.

The whole code is based on the original project from BernK (https://github.com/BerndK/SvgToXaml) whose backend conversion code and main body is preserved.

My changes are more in the UI part to make the application more easy and quick to use. Also some of the features from the original project by BernK has been removed as not required for my needs.